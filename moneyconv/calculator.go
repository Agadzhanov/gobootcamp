package moneyconv

import (
	"flag"
	"fmt"
)

func getCalculationVarsFromCLI() (amount, rate *float64){
	amount = flag.Float64("amount", 0.0, "Currency amount")
	rate = flag.Float64("rate", 0.0, "Currency rate")
	flag.Parse()

	return
}

func calculate(amount, rate *float64) float64{

	return *amount * *rate
}

func echo (sum float64) {
	fmt.Printf("Result: %.2f\n", sum)
}

func Do() {
	echo(calculate(getCalculationVarsFromCLI()))
}
