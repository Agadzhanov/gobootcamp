package lab02

import (
	"flag"
	"fmt"
)

type Employee struct {
	Name string
	Salary float64
	Position string
	Status bool
}

func getFieldsFromCLI() (string, string, bool, float64) {
	name := flag.String("name", "Nameless", "Employee's name")
	position := flag.String("position", "tinker", "Employee's position")
	status := flag.Bool("status", false, "Employee's status")
	salary := flag.Float64("salary", 0.0, "Employee's salary")

	flag.Parse()

	return  *name, *position, *status, *salary
}

func getEmployee(name, position string, status bool, salary float64) Employee{
	return Employee{name,salary,position,status}
}
func echo (employee Employee) {
	var status string
	if employee.Status {
		status = "Active"
	} else {
		status = "Inactive"
	}

	fmt.Printf("Employee: %s, Position: %s, Salary: %.2f, Status: %s\n", employee.Name, employee.Position, employee.Salary, status)
}

func GetAndPrintEmployeeInfo() {
	echo(getEmployee(getFieldsFromCLI()))
}